/*
    (Incomplete) Driver Firmware for LIS2DU12 Accelerometer on ESP32-S2
    Platform:       ESP-IDF version 5.2.2 (Stable)
    Objective:      Power consumption test, across different operation mode configurations (Total Number of Configurations: 26)
    Autor:          Yassir Abbassi
*/

// Libraries
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

/*======================================Function Prototypes======================================*/
void lis2du12_write_transaction(char op, char addr, char din, char mosi2w[], size_t lo2w, spi_transaction_t t,
                            esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void lis2du12_read_transaction(char op, char addr, char mosi[], char miso[], size_t lo, size_t li,
                            spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick);

void lis2du12_Init_Seq(char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle);

void lis2du12_SLEEP_ON(char SLP_odr, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle);

void printCharToBinary(char hex);

void lis2du12_REG_Config(char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle);

void lis2du12_REG_Status(char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle);

float calculate_ratio(char range);

void conversionToG(float ratio, char xyzBuf[], short int XYZ_2c[], float XYZ_g[]);

void NoMotion_angle_coords(float XYZ_g[], float coords[]);

void NoMotion_spherical_coords(float coords[], float angles360[]);

void lis2du12_output_display(float XYZ_g[], float coords[], float angles360[]);

void test_switcher(char ODR_P_MODE[], char Bandwidth[], char RNG_OP[], size_t ODR_count, size_t Bandwidths_count, size_t RNG_count,
                            size_t period, int tick, char mosi2w[], char mosi[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[],
                            float coords[], float angles360[], size_t lo, size_t lxyz, size_t lo2w, spi_transaction_t t, esp_err_t ret,
                            spi_device_handle_t handle, size_t *test_nbr);

void power_consumption_test_presets(char mosi2w[], char mosi[], char miso[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[],
                            float angles360[], size_t lo2w, size_t lo, size_t li, size_t lxyz, spi_transaction_t t, esp_err_t ret,
                            spi_device_handle_t handle, size_t period, unsigned int tick);
/*===============================================================================================*/

/*========================Register Map========================*/
// Configuration
#define LIS2DU12_REG_IF_PU_CTRL	        (0x0C)
#define LIS2DU12_REG_IF_CTRL            (0x0E)
#define LIS2DU12_REG_CTRL1              (0x10)
#define LIS2DU12_REG_CTRL2              (0x11)
#define LIS2DU12_REG_CTRL3              (0x12)
#define LIS2DU12_REG_CTRL4              (0x13)
#define LIS2DU12_REG_CTRL5              (0x14)
#define LIS2DU12_REG_FIFO_CTRL          (0x15)
#define LIS2DU12_REG_FIFO_WTM           (0x16)
#define LIS2DU12_REG_INTERRUPT_CFG      (0x17)
#define LIS2DU12_REG_TAP_THS_X          (0x18)
#define LIS2DU12_REG_TAP_THS_Y          (0x19)
#define LIS2DU12_REG_TAP_THS_Z          (0x1A)
#define LIS2DU12_REG_INT_DUR            (0x1B)
#define LIS2DU12_REG_WAKE_UP_THS        (0x1C)
#define LIS2DU12_REG_WAKE_UP_DUR        (0x1D)
#define LIS2DU12_REG_FREE_FALL          (0x1E)
#define LIS2DU12_REG_MD1_CFG            (0x1F)
#define LIS2DU12_REG_MD2_CFG            (0x20)
#define LIS2DU12_REG_WAKE_UP_SRC        (0x21)
#define LIS2DU12_REG_TAP_SRC            (0x22)
#define LIS2DU12_REG_SIXD_SRC           (0x23)
#define LIS2DU12_REG_ALL_INT_SRC        (0x24)
#define LIS2DU12_REG_STATUS             (0x25)
#define LIS2DU12_REG_FIFO_STATUS1       (0x26)
#define LIS2DU12_REG_FIFO_STATUS2       (0x27)
#define LIS2DU12_REG_OUT_X_L            (0x28)
#define LIS2DU12_REG_OUT_X_H            (0x29)
#define LIS2DU12_REG_OUT_Y_L            (0x2A)
#define LIS2DU12_REG_OUT_Y_H            (0x2B)
#define LIS2DU12_REG_OUT_Z_L            (0x2C)
#define LIS2DU12_REG_OUT_Z_H            (0x2D)
#define LIS2DU12_REG_OUT_T_L            (0x2E)
#define LIS2DU12_REG_OUT_T_H            (0x2F)
#define LIS2DU12_REG_TEMP_OUT_L         (0x30)
#define LIS2DU12_REG_TEMP_OUT_H         (0x31)
#define LIS2DU12_REG_WHO_AM_I           (0x43)
#define LIS2DU12_REG_ST_SIGN            (0x56)
/*============================================================*/

/*=============================================GPIO Definition=============================================*/
// 4-Wire w/ interrupt pin SPI2
#define GPIO_INT  GPIO_NUM_19
#define GPIO_MOSI 11
#define GPIO_MISO 13
#define GPIO_SCLK 12
#define GPIO_CS 10

// Select SPI HOST
#ifdef CONFIG_IDF_TARGET_ESP32
#define SENDER_HOST HSPI_HOST
#else
#define SENDER_HOST SPI2_HOST
#endif
/*=========================================================================================================*/

/* Main application */
void app_main(void)
{
    /*====================================Bus Configurations====================================*/
    // SPI Bus Configuration: MCU 4-Wire GPIOs
    spi_bus_config_t buscfg = {
        .mosi_io_num = GPIO_MOSI,
        .miso_io_num = GPIO_MISO,
        .sclk_io_num = GPIO_SCLK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1
    };

    // SPI Bus Configuration: SPI Slave Device
    spi_device_interface_config_t devcfg = {
        .command_bits = 0,
        .address_bits = 0,
        .dummy_bits = 0,
        .clock_speed_hz = 7000000,          // 7 MHz                (Max 10 Mhz)
        .duty_cycle_pos = 128,              // 50% duty cycle
        .mode = 0,
        .spics_io_num = GPIO_CS,
        .cs_ena_posttrans = 3,              // cs_ena_posttrans = 3 => Keep the CS low 3 cycles after transaction, to stop
        .queue_size = 3                     // slave from missing the last bit when CS has less propagation delay than CLK
    };
    /*==========================================================================================*/

    /*====================================ESP32_S2 SPI Setup====================================*/
    esp_err_t ret;
    spi_device_handle_t handle;
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));

    // Initialize the SPI bus and add EV3635B slave device
    ret = spi_bus_initialize(SENDER_HOST, &buscfg, SPI_DMA_CH_AUTO);
    assert(ret == ESP_OK);
    ret = spi_bus_add_device(SENDER_HOST, &devcfg, &handle);
    assert(ret == ESP_OK);
    /*==========================================================================================*/
    
    /*============================Transaction Buffer Variables============================*/
    char sendBuf2w[2] = {0};            // sendBuf2w: MOSI transaction buffer for 2-byte writing
    size_t lo2w = sizeof(sendBuf2w);

    char sendBuf[1] = {0};              // sendBuf: MOSI transaction buffer for single byte writing
    size_t lo = sizeof(sendBuf);

    char recvBuf[2] = {0};              // recvBuf: MISO transaction buffer for reading
    size_t li = sizeof(recvBuf);

    char xyzBuf[7] = {0};               // recvBuf: MISO transaction buffer for reading raw XYZ output
    size_t lxyz = sizeof(xyzBuf);

    short int XYZ_2c[3] = {0};          // XYZ_2c:  2's Complement reperesentaion of acceleration data
    float XYZ_g[3] = {0};               // XYZ_g:   Decimal conversion of acceleration data (XYZ_g = XYZ_2c / Ratio)

    // Utility Variables
    float coords[3] = {0};               // Stores Euler angles of the accelerometer axises when no motion is applied
    float angles360[3] = {0};            // Stores 360 angles of the accelerometer orientation when no motion is applied

    unsigned int tick = 100;             // tick:        In milliseconds
    size_t period = 10;                  // period:      In seconds
    /*====================================================================================*/

    /*===============================LIS2DU12 Register Initial Configuration===============================*/
    lis2du12_Init_Seq(sendBuf2w, lo2w, t, ret, handle);
    /*=============================================================================================*/

    /*=======================================Power Consumption Test Presets Selector=======================================*/
    power_consumption_test_presets(sendBuf2w, sendBuf, recvBuf, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo2w, lo, li, lxyz,
                                    t, ret, handle, period, tick);
    /*=====================================================================================================================*/

    // END Condition Reached
    ret = spi_bus_remove_device(handle);
    assert(ret == ESP_OK);
}


/* Utility Functions */

// Write Transaction
void lis2du12_write_transaction(char op, char addr, char din, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick)
{
    if (op == 0x0){
        if (lo2w < 2){
            printf("Transaction MOSI buffer length less than 2 bytes. Actual size is %zu.\n", lo2w);
            return;
        }

        mosi2w[0] = op | addr;
        mosi2w[1] = din;
        t.length = lo2w*8;
        t.tx_buffer = mosi2w;
        t.rx_buffer = NULL;
        vTaskDelay(pdMS_TO_TICKS(tick));
        ret = spi_device_transmit( handle, &(t) );

        if(ret != ESP_OK){
            printf("\tR\tADDR(0x%X)%s\n", addr, esp_err_to_name(ret));      // Error Display
        }
    }
}

// Read Transaction
void lis2du12_read_transaction(char op, char addr, char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, unsigned int tick)
{
    if (op == 0x80){
        if (lo != 1 && li < 2){
            printf("Error in transaction buffers size. Mosi size (%zu) \t MISO size (%zu). Should be 1, and at least 2 respectively.\n", lo, li);
            return;
        }

        mosi[0] = op | addr;
        t.length = li*8;
        t.tx_buffer = mosi;
        t.rx_buffer = miso;
        vTaskDelay(pdMS_TO_TICKS(tick));
        ret = spi_device_transmit( handle, &(t) );

        if(ret == ESP_OK){
            if (false)
            {
                printf("\tR\tADDR(0x%X)", addr);
                for (size_t i = 1; i < li; i++)
                {
                    printf("\tN+%zu: OUT(0x%X)", i-1, miso[i]);
                }
                printf("\n");
            }
        }else{
            printf("\tR\tADDR(0x%X)%s\n", addr, esp_err_to_name(ret));
        }
    }
}

// Char to Binary
void printCharToBinary(char hex){
    unsigned int bit;
    for (int i = 7; i >= 0; i--) {
        // Extract the ith bit from the number
        bit = (hex >> i) & 1;

        // Print the bit
        printf("%u", bit);
    }
}

// LIS2DU12 Registery Readers
void lis2du12_REG_Config(char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle){
    unsigned int tick = 25;

    lis2du12_read_transaction(0x80, LIS2DU12_REG_IF_PU_CTRL, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tIF_PU_CTRL (%Xh)\t\t", LIS2DU12_REG_IF_PU_CTRL);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_IF_CTRL, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tIF_CTRL (%Xh)\t\t", LIS2DU12_REG_IF_CTRL);printCharToBinary(miso[1]);printf("\n");
    
    lis2du12_read_transaction(0x80, LIS2DU12_REG_CTRL1, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tCTRL1 (%Xh)\t\t", LIS2DU12_REG_CTRL1);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_CTRL2, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tCTRL2 (%Xh)\t\t", LIS2DU12_REG_CTRL2);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_CTRL3, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tCTRL3 (%Xh)\t\t", LIS2DU12_REG_CTRL3);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_CTRL4, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tCTRL4 (%Xh)\t\t", LIS2DU12_REG_CTRL4);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_CTRL5, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tCTRL5 (%Xh)\t\t", LIS2DU12_REG_CTRL5);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_FIFO_CTRL, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tFIFO_CTRL (%Xh)\t\t", LIS2DU12_REG_FIFO_CTRL);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_FIFO_WTM, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tFIFO_WTM (%Xh)\t\t", LIS2DU12_REG_FIFO_WTM);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_INTERRUPT_CFG, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tINTERRUPT_CFG (%Xh)\t", LIS2DU12_REG_INTERRUPT_CFG);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TAP_THS_X, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTAP_THS_X (%Xh)\t\t", LIS2DU12_REG_TAP_THS_X);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TAP_THS_Y, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTAP_THS_Y (%Xh)\t\t", LIS2DU12_REG_TAP_THS_Y);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TAP_THS_Z, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTAP_THS_Z (%Xh)\t\t", LIS2DU12_REG_TAP_THS_Z);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_INT_DUR, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tINT_DUR (%Xh)\t\t", LIS2DU12_REG_INT_DUR);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_WAKE_UP_THS, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tWAKE_UP_THS (%Xh)\t", LIS2DU12_REG_WAKE_UP_THS);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_WAKE_UP_DUR, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tWAKE_UP_DUR (%Xh)\t", LIS2DU12_REG_WAKE_UP_DUR);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_FREE_FALL, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tFREE_FALL (%Xh)\t\t", LIS2DU12_REG_FREE_FALL);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_MD1_CFG, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tMD1_CFG (%Xh)\t\t", LIS2DU12_REG_MD1_CFG);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_MD2_CFG, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tMD2_CFG (%Xh)\t\t", LIS2DU12_REG_MD2_CFG);printCharToBinary(miso[1]);printf("\n");
}

void lis2du12_REG_Status(char mosi[], char miso[], size_t lo, size_t li, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle){
    unsigned int tick = 25;

    lis2du12_read_transaction(0x80, LIS2DU12_REG_WAKE_UP_SRC, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tWAKE_UP_SRC (%Xh)\t", LIS2DU12_REG_WAKE_UP_SRC);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TAP_SRC, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTAP_SRC (%Xh)\t\t", LIS2DU12_REG_TAP_SRC);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_SIXD_SRC, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tSIXD_SRC (%Xh)\t\t", LIS2DU12_REG_SIXD_SRC);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_ALL_INT_SRC, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tALL_INT_SRC (%Xh)\t", LIS2DU12_REG_ALL_INT_SRC);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_STATUS, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tSTATUS (%Xh)\t\t", LIS2DU12_REG_STATUS);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_FIFO_STATUS1, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tFIFO_STATUS1 (%Xh)\t", LIS2DU12_REG_FIFO_STATUS1);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_FIFO_STATUS2, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tFIFO_STATUS2 (%Xh)\t", LIS2DU12_REG_FIFO_STATUS2);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_X_L, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_X_L (%Xh)\t\t", LIS2DU12_REG_OUT_X_L);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_X_H, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_X_H (%Xh)\t\t", LIS2DU12_REG_OUT_X_H);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_Y_L, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_Y_L (%Xh)\t\t", LIS2DU12_REG_OUT_Y_L);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_Y_H, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_Y_H (%Xh)\t\t", LIS2DU12_REG_OUT_Y_H);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_Z_L, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_Z_L (%Xh)\t\t", LIS2DU12_REG_OUT_Z_L);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_Z_H, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_Z_H (%Xh)\t\t", LIS2DU12_REG_OUT_Z_H);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_T_L, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_T_L (%Xh)\t\t", LIS2DU12_REG_OUT_T_L);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_T_H, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tOUT_T_H (%Xh)\t\t", LIS2DU12_REG_OUT_T_H);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TEMP_OUT_L, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTEMP_OUT_L (%Xh)\t", LIS2DU12_REG_TEMP_OUT_L);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_TEMP_OUT_H, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tTEMP_OUT_H (%Xh)\t", LIS2DU12_REG_TEMP_OUT_H);printCharToBinary(miso[1]);printf("\n");

    lis2du12_read_transaction(0x80, LIS2DU12_REG_WHO_AM_I, mosi, miso, lo, li, t, ret, handle, tick);
    printf("\n\tWHO_AM_I (%Xh)\t\t", LIS2DU12_REG_WHO_AM_I);printCharToBinary(miso[1]);printf("\n");
}

// LIS2DU12 Initialization Sequence
void lis2du12_Init_Seq(char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle){
    unsigned int tick = 25;

    lis2du12_write_transaction(0x0, LIS2DU12_REG_IF_PU_CTRL, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_IF_CTRL, 0x7, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL1, 0x17, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL2, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL3, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL4, 0x40, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL5, 0xB3, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_FIFO_CTRL, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_FIFO_WTM, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_INTERRUPT_CFG, 0x42, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_TAP_THS_X, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_TAP_THS_Y, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_TAP_THS_Z, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_INT_DUR, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_WAKE_UP_THS, 0xC0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_WAKE_UP_DUR, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_FREE_FALL, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_MD1_CFG, 0x0, mosi2w, lo2w, t, ret, handle, tick);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_MD2_CFG, 0x0, mosi2w, lo2w, t, ret, handle, tick);
}

// LIS2DU12 Inactivity/Activity Function Enable
void lis2du12_SLEEP_ON(char SLP_odr, char mosi2w[], size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle){
    if (SLP_odr == 0x0 || SLP_odr == 0x40 || SLP_odr == 0x80 || SLP_odr == 0xC0)
    {
        lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL4, SLP_odr, mosi2w, lo2w, t, ret, handle, 25);
    }else
    {
        printf("Selected Sleep ODR command is not valid\n");
        return;
    }
    lis2du12_write_transaction(0x0, LIS2DU12_REG_WAKE_UP_THS, 0x7F, mosi2w, lo2w, t, ret, handle, 25);
    lis2du12_write_transaction(0x0, LIS2DU12_REG_WAKE_UP_DUR, 0x6F, mosi2w, lo2w, t, ret, handle, 25);
}

// Ratio Calculation
float calculate_ratio(char range){
    float ratio = 2048.0f;

    // Adjust the ratio based on the range chosen value
    switch (range){
        case 0x0:
            ratio /= 2.0f;
            break;
        case 0x1:
            ratio /= 4.0f;
            break;
        case 0x2:
            ratio /= 8.0f;
            break;
        case 0x3:
            ratio /= 16.0f;
            break;
        default:
            // Handle invalid range value
            return 0.0f;
    }

    return ratio;
}

// Acceleration Calculation
void conversionToG(float ratio, char xyzBuf[], short int XYZ_2c[], float XYZ_g[]){
    XYZ_2c[0] = ( (xyzBuf[1]) + (xyzBuf[2] << 8) );
    XYZ_2c[1] = ( (xyzBuf[3]) + (xyzBuf[4] << 8) );
    XYZ_2c[2] = ( (xyzBuf[5]) + (xyzBuf[6] << 8) );

    XYZ_2c[0] = XYZ_2c[0] >> 4;
    XYZ_2c[1] = XYZ_2c[1] >> 4;
    XYZ_2c[2] = XYZ_2c[2] >> 4;

    XYZ_g[0] = XYZ_2c[0] / ratio;
    XYZ_g[1] = XYZ_2c[1] / ratio;
    XYZ_g[2] = XYZ_2c[2] / ratio;
}

// Angles in Degrees (No Motion Case)
void NoMotion_angle_coords(float XYZ_g[], float coords[]){
    // Angle Coordinates for X axis
    if (XYZ_g[0] == 1 || XYZ_g[0] == -1)
    {
        coords[0] = XYZ_g[0];
    }else
    {
        coords[0] = fmodf(XYZ_g[0], 1.0f);
    }

    // Angle Coordinates for Y axis
    if (XYZ_g[1] == 1 || XYZ_g[1] == -1)
    {
        coords[1] = XYZ_g[1];
    }else
    {
        coords[1] = fmodf(XYZ_g[1], 1.0f);
    }

    // Angle Coordinates for Z axis
    if (XYZ_g[2] == 1 || XYZ_g[2] == -1)
    {
        coords[2] = XYZ_g[2];
    }else
    {
        coords[2] = fmodf(XYZ_g[2], 1.0f);
    }
}

// 360 Degrees convertion of angles computed by NoMotion_angle_coords()
void NoMotion_spherical_coords(float coords[], float angles360[]){
    angles360[0] = (180.0/M_PI) * (atan2(-coords[1]*90, -coords[2]*90) + M_PI);
    angles360[1] = (180.0/M_PI) * (atan2(-coords[0]*90, -coords[2]*90) + M_PI);
    angles360[2] = (180.0/M_PI) * (atan2(-coords[1]*90, -coords[0]*90) + M_PI);
}

// Output Display function
void lis2du12_output_display(float XYZ_g[], float coords[], float angles360[]){
    // Euler Angles
    NoMotion_angle_coords(XYZ_g, coords);
    NoMotion_spherical_coords(coords, angles360);
    //printf("Euler Angles: \t%.2f\t%.2f\t%.2f\n", coords[0]*90, coords[1]*90, coords[2]*90);
    //printf("360 Angles: \t%.2f\t%.2f\t%.2f\n", angles360[0], angles360[1], angles360[2]);
    
    // Acceleration
    printf("Acceleration: \t%.2fg\t%.2fg\t%.2fg\n", XYZ_g[0], XYZ_g[1], XYZ_g[2]);
}

// Test Configuration Switcher
void test_switcher(char ODR_P_MODE[], char Bandwidth[], char RNG_OP[], size_t ODR_count, size_t Bandwidths_count, size_t RNG_count, size_t period, int tick, char mosi2w[], char mosi[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[], float angles360[], size_t lo, size_t lxyz, size_t lo2w, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, size_t *test_nbr){
    // Loop for output display's variables
    size_t count = period * (1000/tick);             // For instance, if tick = 200, then count = 20 * (1000/tick) = 100 readings per seconds
    float ratio;

    /*=========================================String Indicator for Selected Test Preset=========================================*/
    char ODR[8];                        // LIS2DU12_REG_CTRL5
    char Power_Mode[16];                // LIS2DU12_REG_CTRL5
    char Band[7];                       // LIS2DU12_REG_CTRL5
    char RNG[7];                        // LIS2DU12_REG_CTRL5
    /*===========================================================================================================================*/

    /*======================================================Test Loop======================================================*/
    // Switching to sleep mode is required before the start of test
    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL5, 0x0, mosi2w, lo2w, t, ret, handle, 25);

    // Loop over ODR and Power Modes
    for (size_t i = 0; i < ODR_count; i++)
    {
        switch (ODR_P_MODE[i]){
        case 0x10:
            strcpy(ODR, "1.6 Hz");
            strcpy(Power_Mode, "Ultra-Low Power");
            break;
        
        case 0x20:
            strcpy(ODR, "3 Hz");
            strcpy(Power_Mode, "Ultra-Low Power");
            break;

        case 0x30:
            strcpy(ODR, "6 Hz");
            strcpy(Power_Mode, "Ultra-Low Power");
            break;
        
        case 0x40:
            strcpy(ODR, "6 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;
        
        case 0x50:
            strcpy(ODR, "12.5 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;

        case 0x60:
            strcpy(ODR, "25 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;

        case 0x70:
            strcpy(ODR, "50 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;

        case 0x80:
            strcpy(ODR, "100 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;

        case 0x90:
            strcpy(ODR, "200 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;
        
        case 0xA0:
            strcpy(ODR, "400 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;

        case 0xB0:
            strcpy(ODR, "800 Hz");
            strcpy(Power_Mode, "Normal Mode");
            break;
        

        default:
            strcpy(ODR, "ERR");
            strcpy(Power_Mode, "ERR");
            break;
        }

        // Loop over bandwidths
        for (size_t j = 0; j < Bandwidths_count; j++)
        {
            switch (Bandwidth[j]){
            case 0x0:
                strcpy(Band, "ODR/2");
                break;

            case 0x4:
                strcpy(Band, "ODR/4");
                break;

            case 0x8:
                strcpy(Band, "ODR/8");
                break;

            case 0xC:
                strcpy(Band, "ODR/16");
                break;
                
            default:
                strcpy(Band, "ERR");
                break;
            }

            // Loop over ranges
            for (size_t u = 0; u < RNG_count; u++)
            {
                ratio = calculate_ratio(RNG_OP[u]);

                switch (RNG_OP[u]){
                case 0x0:
                    strcpy(RNG, "+-2 G");
                    break;

                case 0x1:
                    strcpy(RNG, "+-4 G");
                    break;

                case 0x2:
                    strcpy(RNG, "+-8 G");
                    break;

                case 0x3:
                    strcpy(RNG, "+-16 G");
                    break;
                    
                default:
                    strcpy(RNG, "ERR");
                    break;
                }
                
                // Writing operation for setting operational mode. THIS ALWAYS SHOULD BE THE LAST TRANSACTION
                lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL5, ODR_P_MODE[i] + Bandwidth[j] + RNG_OP[u], mosi2w, lo2w, t, ret, handle, 25);

                // Reading Loop
                printf("\n\n\tTest Configs %zu: %s | %s | %s | 12-bit | %s\n", *test_nbr, ODR, Power_Mode, Band, RNG); (*test_nbr)++;

                    for (size_t x = 0; x < count; x++)
                    {
                        lis2du12_read_transaction(0x80, LIS2DU12_REG_OUT_X_L, mosi, xyzBuf, lo, lxyz, t, ret, handle, tick);
                        conversionToG(ratio, xyzBuf, XYZ_2c, XYZ_g);
                        lis2du12_output_display(XYZ_g, coords, angles360);            // Optional
                    }

                    // Recommended!! Switching to sleep mode after every test configuration
                    lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL5, 0x0, mosi2w, lo2w, t, ret, handle, 25);
                    /*=====================================================================================================================*/
            }
        }
    }
}

// Power Modes Test
void power_consumption_test_presets(char mosi2w[], char mosi[], char miso[], char xyzBuf[], short int XYZ_2c[], float XYZ_g[], float coords[], float angles360[], size_t lo2w, size_t lo, size_t li, size_t lxyz, spi_transaction_t t, esp_err_t ret, spi_device_handle_t handle, size_t period, unsigned int tick){

    /*======================================================Preset Test Variables Array======================================================*/
    // Inactivity/Activity ODRs per Power Modes & Ranges                // SNF ODR for ULP, LP, and P power modes respectively
    char ODR_Inactivity[3] = {0x40, 0x80, 0xC0};                        // {6, 3, 1.6}                                              (Hz)
    char RNG_SNF[1] = {0x3};                                            // Ranges for SNF: {+- 16}                                  (G)

    // Ulralow Power and Normal Modes ODRs per Ranges and bandwidths
    char ODR_WAK_MODE[11] = {0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x90, 0xA0, 0xB0};
    /*Power Modes:           ULP   ULP   ULP   NM    NM    NM    NM    NM    NM    NM   NM      ULP: Ultralow-Power     NM: Normal Mode */
    /*ODR (Hz):          -   1.6    3     6    6     12.5  25    50    100   200   400  800                                             */

    char RNG_WAK[2] = {0x0, 0x3};                                       // Ranges for C/SWake: {+-2, +-16}                          (G)
    char Bandwidth[1] = {0x0};                                          // Bandwidths: {ODR/2}                                      (Hz)
    /*=======================================================================================================================================*/
    
    // Analysis Time Measurment
    TickType_t start_tick = xTaskGetTickCount();                        // Get start tick counts
    size_t count = period * (1000/tick);
    size_t test_nbr = 1;                                                // Initialize test number

    // Switch to Power-Down
    lis2du12_write_transaction(0x40, LIS2DU12_REG_CTRL5, 0x0, mosi2w, lo2w, t, ret, handle, 25);

    // Inactivity/Activity Modes
    for (size_t i = 0; i < 3; i++)
    {
        printf("\n\n\tTest Configs %zu: %X | Inactivity | ODR/2 | 12-bit | +-16g\n", test_nbr, ODR_Inactivity[i]); (test_nbr)++;
        lis2du12_write_transaction(0x0, LIS2DU12_REG_CTRL5, Bandwidth[0] + RNG_SNF[0], mosi2w, lo2w, t, ret, handle, 25);
        lis2du12_SLEEP_ON(ODR_Inactivity[i], mosi2w, lo2w, t, ret, handle);

        for (size_t i = 0; i < count; i++)
        {
            printf("%zu ",i+1);
            vTaskDelay(pdMS_TO_TICKS(tick));                            // If tick = 200, then i_max = 20 * (1000/tick) = 100
        }
    }

    // Sleep
    lis2du12_write_transaction(0x40, LIS2DU12_REG_CTRL5, 0x0, mosi2w, lo2w, t, ret, handle, 25);
    printf("\n\n\tTest Configs %zu: Sleep | No Configuration\n", test_nbr);
    for (size_t i = 0; i < count; i++)
    {
        // If tick = 200, then i_max = 20 * (1000/tick) = 100
        vTaskDelay(pdMS_TO_TICKS(tick));
    }
    test_nbr++;

    // Wake-up Modes
    test_switcher(ODR_WAK_MODE, Bandwidth, RNG_WAK, sizeof(ODR_WAK_MODE), sizeof(Bandwidth), sizeof(RNG_WAK), period, tick, mosi2w, mosi, xyzBuf, XYZ_2c, XYZ_g, coords, angles360, lo, lxyz, lo2w, t, ret, handle, &test_nbr);

    // Print Total Analysis Time
    TickType_t end_tick = xTaskGetTickCount();                          // Get end tick count
    TickType_t elapsed_ticks = end_tick - start_tick;                   // Calculate elapsed ticks
    uint32_t elapsed_time_ms = elapsed_ticks * portTICK_PERIOD_MS;      // Convert ticks to milliseconds
    
    int minutes = elapsed_time_ms / (60 * 1000);                        // Extract minutes
    int seconds = (elapsed_time_ms % (60 * 1000)) / 1000;               // Extract seconds
    int milliseconds = elapsed_time_ms % 1000;                          // Extract milliseconds
    printf("\n\n\t===== Elapsed time: %d mins, %d secs, %d msecs =====\n\n", minutes, seconds, milliseconds);
}