# Power Consumption Analysis of LIS2DU12 Accelerometers

## Overview

This project aims to perform a comprehensive power consumption analysis of the EV3635B and LIS2DU12 accelerometers under stress conditions using the ESP-IDF framework. By running both accelerometers in continuous mode with 12-bit resolution and carefully selected matching configurations, we seek to gather essential data for a Modular Parametric Power Saving Design (MPPS). This analysis will help determine which accelerometer offers better power efficiency, providing critical insights for 68.75% to 100% of scenarios.

(Link to the EV3635B power consumption test project: https://gitlab.com/embedded-systems7413845/esp-idf/esp32s2-ev3635b-accelerometer-driver)

## Objectives

- Conduct a stress test on EV3635B and LIS2DU12 accelerometers to measure their power consumption in continuous mode.
- Utilize a consistent 12-bit resolution for both accelerometers.
- Evaluate 91 different configurations for the EV3635B and 26 configurations for the LIS2DU12.
- Gather typical power consumption values that will inform a custom power-saving design.

## Why Stress Test?

Stress testing both accelerometers reveals elementary power consumption values without relying on pre-built power-saving functions or custom firmware functions. This approach provides a clear baseline for comparison:
- **Elementary Information**: Essential for designing a Modular Parametric Power Saving Design (MPPS).
- **Theoretical Comparison**: Typical current consumption values under stress conditions can guide the selection of the more power-efficient accelerometer.

## Limitations

The analysis may not be decisive if the pre-built power-saving functions of the LIS2DU12 outperform a custom firmware power-saving configuration. However, stress testing still provides a strong foundation for most scenarios.

## Documentation

Summary of analysis is documented and shared in this repository. Please refer to:

https://gitlab.com/embedded-systems7413845/esp-idf/esp32s2-ev3635b-accelerometer-driver/-/tree/main/Documentation?ref_type=heads